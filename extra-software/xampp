#!/bin/bash
#
# #################################################################
#
# Cool Debian 12 unstable (sid branch)
# Written to be used on 64 bits computers
#
# Author            : TheGreatYellow67 (TgY67)
# GitLab Repo       : https://gitlab.com/thegreatyellow67/
#
# File              : xampp
# Start date        : 10/03/2023
# Last modified date: 24/03/2023
#
# #################################################################

#
# Functions and ANSI codes
#
if [ ! -n "${root_folder}" ]; then
	source ../functions
	source ../ansi-codes
else
	source ${root_folder}/functions
	source ${root_folder}/ansi-codes
fi

#
# Check if user has sudo privileges
#
check_sudo

#
# #################################################################
#
RUN_FILE="https://netcologne.dl.sourceforge.net/project/xampp/XAMPP%20Linux/8.2.0/xampp-linux-x64-8.2.0-0-installer.run"
RUN_TMP_FILENAME="xampp.run"
APP_NAME="XAMPP"

clear
printf "\n"
msg_info "=> Installing ${APP_NAME}..."

if [ ! -d "/opt/lampp" ]; then

	if [ -f /tmp/${RUN_TMP_FILENAME} ]; then
		rm /tmp/${RUN_TMP_FILENAME}
	fi

	wget ${RUN_FILE} -O /tmp/${RUN_TMP_FILENAME}

    ## The exit status of the last command run is
    ## saved automatically in the special variable $?.
    ## Therefore, testing if its value is 0, is testing
    ## whether the last command ran correctly.
    if [[ $? > 0 ]]
    then
        msg_error "=> Package ${RUN_FILE} not found, exiting!"
    else
		cd /tmp
		chmod +x ${RUN_TMP_FILENAME}
		sudo ./${RUN_TMP_FILENAME}

		rm /tmp/${RUN_TMP_FILENAME}

		msg_success "=> ${APP_NAME} installed!"
		printf "\n"
    fi

else

	msg_warning "=> ${APP_NAME} is already installed in your system!"
	printf "\n"

fi

printf "\n"
read -n 1 -s -r -p " Press any key to continue"
